<?php



class Core {

   protected $controladorActual = "Paginas"; //Controlador por defecto
   
    protected $metodoActual = "index"; // Método por defecto
   
    protected $parametros = []; // Por defecto no hay parámetros
   
    public function __construct() {
   
        $url = $this->getUrl(); //Recoge la URL
        if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) { //Comprueba que esista el controlador y si existe cambia el valor por defecto del controlador al requerido por la URL
        $this->controladorActual = ucwords($url[0]);
        unset($url[0]);
        }
        require_once '../app/controllers/' . $this->controladorActual . '.php'; //Llamamos y requerimos al controlador ya sea el por defecto o el de la url si existiera.
        $this->controladorActual = new $this->controladorActual; //Instanciamos el controlador

        if (isset($url[1])) { //Comprobamos si la url dispone de una segunda parte
            if (method_exists($this->controladorActual, $url[1])) { //Si tuviese segunda parte, buscamos si existe el metodo al que esta intentando acceder en el controlador que hemos cargado previamente
            $this->metodoActual = $url[1]; //Guardamos el valor del metodo existente, si no entrase aqui el valor del metodo por defecto es index.
            unset($url[1]);
            }
            }

            $this->parametros = $url ? array_values($url) : []; //Carga en la variable paramentros el resto de valores de la url (sin el controlador y el metodo si hubiese, ya que los hemos unsetado previamente)
            call_user_func_array([$this->controladorActual, $this->metodoActual], $this->parametros); //Esta función se encarga cuando inicia de llamar al controlador y el metodo ( o el de defecto si no existiese o estuviese mal) con los respectivos parametros para el metodo si los necesitase de la url solicitada, de esta forma no tenemos que comprobar nosotros uno por uno a que llamar.
   
    }
   
    public function getURL(){
   
        if (isset($_GET['url'])) { 

            $url = rtrim($_GET['url'], '/');
           
            $url = filter_var($url, FILTER_SANITIZE_URL);
           
            $url = explode('/', $url);
           
            } else {
           
            $url[0] = 'Paginas';
           
            }
            return $url;
   
    }
   
   }

?>