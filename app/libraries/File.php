<?php
require_once APPROOT. "/exceptions/FileException.php";

class File extends FileException
    {

        private $fileName;
        private $allowed;

        public function __construct($fileName, $allowed)
            {   
                $this->fileName = $fileName;
                $this->allowed = $allowed;
            }

        public function fileChecker(){
            if ($this->fileName['error'] !== UPLOAD_ERR_OK) {
                switch ($this->fileName['error']) {
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                throw new FileException('El fichero es demasiado grande. Tamaño máximo: ' . ini_get('upload_max_filesize'));
                break;
                case UPLOAD_ERR_PARTIAL:
                throw new FileException('No se ha podido subir el fichero completo');
                break;
                default:
                throw new FileException('Error al subir el fichero');
                break;
                }
                }
                else {
                
                if (in_array($this->fileName['type'], $this->allowed) === false) {
                    throw new FileException('Tipo de fichero no soportado');
                } else if (is_uploaded_file($this->fileName['tmp_name']) === false) {
                    throw new FileException('El archivo no se ha subido mediante un formulario');
                } else if (move_uploaded_file($this->fileName['tmp_name'], '../public/img/' . $this->fileName['name']) === false) {
                    throw new FileException('Ocurrió algún error al subir el fichero. No pudo guardarse.');
                }
                }
            }
}
                ?>