<?php include_once APPROOT. "/views/partials/header.php"; ?>
<?php include_once APPROOT. "/views/partials/navbar.php"; ?>
<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
        <?= (string) flash() ?>
            <h2> Crear una cuenta</h2>
            <div class="hola"></div>
            <p>Por favor llena los campos para poder registrarse</p>
            <form method="POST" action="">
                <div class="form-group">
                    <label for="name">Nombre: <sup>*</sup></label>
                    <input type="text" name="name" class="form-control <?php if ($data['name_err'] != ""){ echo "is-invalid";}?>" value="<?= $data['name']?>">                    
                </div>
                <div class="form-group">
                    <label for="email">Email: <sup>*</sup></label>
                    <input type="email" name="email" class="form-control <?php if ($data['email_err'] != ""){ echo "is-invalid";}?>" value="<?= $data['email']?>">                    
                </div>
                <div class="form-group">
                    <label for="password">Contraseña: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control <?php if ($data['password_err'] != ""){ echo "is-invalid";}?>" value="<?= $data['password']?>">
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirmar contraseña: <sup>*</sup></label>
                    <input type="password" name="confirm_password" class="form-control <?php if ($data['confirm_password_err'] != ""){ echo "is-invalid";}?>" value="<?= $data['confirm_password']?>">                    
                </div>
                <div class="row">
                    <div class="col">
                        <a href="<?=  URLROOT."users/login"; ?>">¿Ya tienes cuenta? Inicia sesión</a>
                    </div>
                    <div class="col">
                        <input type="submit" value="Registrar" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include_once APPROOT. "/views/partials/footer.php"; ?>