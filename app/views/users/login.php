<?php include_once APPROOT. "/views/partials/header.php"; ?>
<?php include_once APPROOT. "/views/partials/navbar.php"; ?>

<div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
            <div class="flashes">
                <?= (string) flash() ?>
            </div>
               
                <h2>Iniciar sesión</h2>
                <p>Por favor, introduzca su correo y contraseña</p>
                <form method="POST" action="">
                    <div class="form-group">
                        <label for="email">Email: <sup>*</sup></label>
                        <input type="email" name="email" class="form-control" placeholder="Su correo electrónico" value="<?php $data['email'] ?>">                        
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña: <sup>*</sup></label>
                        <input type="password" name="password" class="form-control" placeholder="Su contraseña" value="<?php $data['password']?>">                        
                    </div>
                    <div class="row">
                        <div class="col">
                            <a href="<?= URLROOT. "users/register" ?>">¿No tienes cuenta? Regístrate</a>
                        </div>
                        <div class="col">
                            <input type="submit" value="Iniciar sesión" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include_once APPROOT. "/views/partials/footer.php"; ?>