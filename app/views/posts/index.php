<?php include_once APPROOT. "/views/partials/header.php"; ?>
<?php include_once APPROOT. "/views/partials/navbar.php"; ?>
<div class="row mb-3">
<div class="col-md-6">
<h1>Publicaciones</h1>
<?= (string) flash() ?>
</div>
<div class="col-md-6">
<a class="btn btn-primary pull-right" href="<?= URLROOT.'posts/add' ?>" role="button">
<i class="fas fa-pencil-alt"></i> Crear publicación
</a>
</div>
</div>

<table>

<?php  foreach ($data as $post): ?>
    
           <tr>
           <?php if($post->image != ""){echo '<td><img width="200px" src="'.URLROOT.'img/'.$post->image.'"></td>"';}?>
               <td><?= $post->title; ?></td>
               <td><?= $post->body; ?></td>
               <td><?= $post->name; ?></td>
               <td><?= $post->postCreatedAt; ?></td>
               <td><form method="GET" action=<?= URLROOT."posts/show"?>><input type="hidden" name="post_id" value="<?php echo $post->postId; ?>"><button type="submit">Más</button></form></td>
           </tr>

<?php endforeach; ?>
</table>  
<?php include_once APPROOT. "/views/partials/footer.php"; ?>
