<?php include_once APPROOT. "/views/partials/header.php"; ?>
<?php include_once APPROOT. "/views/partials/navbar.php"; ?>
<a class="btn btn-warning pull-right" href="<?= URLROOT. "posts/index" ?>" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<?= (string) flash() ?>
<br>
<div class="row mb-3">
    <div class="col-md-12">
        <h1><?= $data->title; ?></h1>

        <div class="bg-secondary text-white p-2 mb-3">
            Creado por: <?= $data->name; ?>  el <?= $data->postCreatedAt; ?>
        </div>
        <?php if($data->image != ""){echo '<img src="'.URLROOT.'img/'.$data->image.'">';}?>
        <p>

<?= $data->body; ?>

        </p>
        <?php if($_SESSION['id'] == $data->userId){
            echo '<hr>
            <div class="row">
            <div class="col">
            <form action="'.URLROOT.'posts/edit" method="GET">      
                <input type="hidden" name="post_id" value="'.$data->postId.'">                  
                <button type="submit" class="btn btn-success btn-block">
                <i class="fas fa-edit"></i> Editar post
            </button>
            </form>
            </div>
                <div class="col">
                    <form action="'.URLROOT.'posts/delete" method="POST">   
                    <input type="hidden" name="post_id" value="'.$data->postId.'">                     
                        <button type="submit" class="btn btn-danger btn-block">
                            <i class="fas fa-trash"></i> Borrar post
                        </button>
                    </form>
                </div>
            </div>';}?>
        
    </div>
</div>
<?php include_once APPROOT. "/views/partials/footer.php"; ?>