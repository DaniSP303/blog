<?php

class User{

private $db;

private $name;

private $email;

private $password;


public function __construct($name = '', $email = '', $password = ''){

    $this->db = new Database();
    $this->name = $name;
    $this->email = $email;   
    $this->password = $password;

}

public function getName(){

    return $this->name;

}

public function getEmail(){
    
    return $this->email;

}

public function getPassword(){

    return $this->password;
    
}

public function setName($name){

     $this->name = $name;

}

public function setEmail($email){
    
    $this->email = $email;

}

public function setPassword($password){

    $this->password = $password;
    
}


public function findUserByEmail($mail){

 $this->db->query('SELECT * FROM users where email = :email');

 $this->db->bind(':email', $mail);

 $results = $this->db->resultOne();

 return $results;
}

public function userExist($mail){

    $this->db->query('SELECT * FROM users where email = :email');
   
    $this->db->bind(':email', $mail);
   
    $results = $this->db->resultSet("User");
   
    return $results;
   }

public function register($data){

 $this->db->query('insert into users (name,email,password) Values (:nm,:ml,:pw)');

 $this->db->bind(':nm', $data['name']);

 $this->db->bind(':ml', $data['email']);

 $this->db->bind(':pw', $data['password']);

 return $this->db->execute();

}

public function login($data){

    $result = $this->findUserByEmail($data['email']);
    var_dump($result);
   
    if(password_verify($data['password'], $result->password)){
        return $result;
    }else{
        return false;
    }
   
   }


public function rowCount($mail){

return  count($this->userExist($mail));
 
}

}
?>