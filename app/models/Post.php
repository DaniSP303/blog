<?php

class Post{

private $db;

public function __construct(){

    $this->db = new Database();


}

public function getPosts(){

 $this->db->query('SELECT *,
 posts.id as postId,
 posts.created_at as postCreatedAt,
 users.id as userId ,
 users.created_at as userCreatedAt
 FROM posts
 INNER JOIN users
 ON posts.user_id = users.id
 ORDER BY posts.created_at DESC');

$results = $this->db->resultSet("Post");

 return $results;
}

public function getPost($postID){

    $this->db->query('SELECT *,
    posts.id as postId,
    posts.created_at as postCreatedAt,
    users.id as userId ,
    users.created_at as userCreatedAt
    FROM posts
    INNER JOIN users
    ON posts.user_id = users.id
    where posts.id = :postID');

    $this->db->bind(':postID', $postID);
   
    $results = $this->db->resultOne("Post");
   
    return $results;
   }

   public function addPost($data){

    $this->db->query('insert into posts (user_id,title,body,image, created_at) Values (:ui,:ti,:bo,:im, :ca)');
   
    $this->db->bind(':ui', $_SESSION['id']);
   
    $this->db->bind(':ti', $data['title']);
   
    $this->db->bind(':bo', $data['body']);

    $this->db->bind(':im', $_FILES['image']['name']);

    $this->db->bind(':ca', date("Y/m/d"));
   
    return $this->db->execute();
   
   }

   public function updatePost($data){

    $this->db->query('UPDATE posts SET title = :ti, body = :bo WHERE id = :id');
   
    $this->db->bind(':ti', $data['title']);
   
    $this->db->bind(':bo', $data['body']);

    $this->db->bind(':id', $data['id']);
   
    return $this->db->execute();
   
   }

   public function deletePost($id){

    $this->db->query('DELETE from posts where id = :id');

    $this->db->bind(':id', $id);
   
    return $this->db->execute();
   
   }

}
?>