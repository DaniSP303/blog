<?php
if( !session_id() ) session_start();

require_once 'vendor/autoload.php';

require_once 'helpers/utils.php';

use Dotenv\Dotenv;
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

require_once 'config/config.php';
spl_autoload_register(function ($className) {
    require_once 'libraries/'.$className.'.php';
    });


?>