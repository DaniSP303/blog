<?php
/* require '../app/libraries/Controller.php'; */
use \Tamtamchik\SimpleFlash\Flash;

class Users extends Controller{

  
    

    public function __construct() {
   
        $this->user = $this->model("User");
   
    }

    public function register(){
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = [
                'name' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''
                ];

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data['name'] = trim($_POST['name']);
            $data['email'] = trim($_POST['email']);
            $data['password'] = trim($_POST['password']);
            $data['confirm_password'] = trim($_POST['confirm_password']);

            if($data['name'] == ""){
                $data['name_err'] = "Hace falta un nombre";
            }else{
                $data['name_err'] = "";
            }

            if($data['email'] == ""){
                $data['email_err'] = "Email no válido";
            }else{
                $data['email_err'] = "";
            }
            
            if($data['password'] == "" || strlen($data['password']) < 6 )
            {
                $data['password_err'] = "Contraseña no válida";
            }else{
                $data['password_err'] = "";
            }
           
            if($data['confirm_password'] == "" || $data['password'] != $data['confirm_password'])
            {
                $data['confirm_password_err'] = "Contraseña de confirmación no válida";
            }else{
                $data['confirm_password_err'] = "";
            }

            $count = $this->user->rowCount($data['email']);

            if( $data['name_err'] == "" && $data['email_err'] == "" && $data['password_err'] == "" && $data['confirm_password_err'] == "" && $count == 0){

                $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
                $result = $this->user->register($data);
                if ($result){
                    $helper = new Utils;
                    $flash = new Flash();
                    $flash->message('Ya estás registrado y puedes iniciar sesión.', 'info');
                    $helper->urlHelper("users/login");
                }

            }
            else    
            {
                if ($count > 0){
                 $flash = new Flash();
                $flash->message('Esa cuenta ya existe', 'info'); 
                }

                $this->view('users/register', $data);
            }
            
        } else {
            $data = [
            'name' => '',
            'email' => '',
            'password' => '',
            'confirm_password' => '',
            'name_err' => '',
            'email_err' => '',
            'password_err' => '',
            'confirm_password_err' => ''
            ];
        $this->view('users/register', $data);
        }

     }

    public function login(){

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => ''               
                ];
                

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
           
            $data['email'] = trim($_POST['email']);
            $data['password'] = trim($_POST['password']);
  
            if($data['email'] == ""){
                $data['email_err'] = "Email no válido";
            }else{
                $data['email_err'] = "";
            }
            
            if($data['password'] == "" || strlen($data['password']) < 6 )
            {
                $data['password_err'] = "Contraseña no válida";
            }else{
                $data['password_err'] = "";
            }
            
            if( $data['email_err'] == "" && $data['password_err'] == "" && strlen($data['password']) >= 6){
                $result = $this->user->login($data);
                
                //ESTE IF PODRIA SER PARTE DE LA CONDICION DE ARRIBA?????
                if ($this->user->rowCount($data['email']) > 0) {
                    // Lo haremos más adelante
                    $data['email_err'] ="";
                 } else {
                    $data['email_err'] = 'El usuario no se ha encontrado';
                 }

                if (isset($result)) {
                    // Lo haremos más adelante: creamos variables de sesión
                    $data['password_err'] = "";
                    $helper = new Utils;
                    $helper->createUserSession($result);
                    
                } else {
                    $data['password_err'] = 'Contraseña incorrecta';
                    $this->view('users/login', $data);
}
            }
            else
            {
                $this->view('users/login', $data);
            }
            
        } else {
            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => ''               
                ];
            $this->view('users/login', $data);
        }

   
   }


public function logout(){
    $helper = new Utils;
    $helper->logout();
}

}

?>